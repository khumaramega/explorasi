const mega = require('readline')
const rl = mega.createInterface({
    input: process.stdin,
    output: process.stdout

})


function calculateRectangularPrism(length, width, height) {
    return length * width * height
}

// cara test fungsi console.log(calculateRectangularPrism(10,10,10)); 

function inputLength(){
    rl.question(`Length: `, length => {
        if(!isNaN(length)) {
            inputWidth(length)
        } else {
            console.log("Length must be number!")
            inputLength()
        }
    })
}

function inputWidth(length){
    rl.question(`Width: `, width => {
        if(!isNaN(width)) {
            inputHeight(length, width)
        } else {
            console.log("Width must be number!");
            inputWidth(length)
        }
    })
}

function inputHeight(length, width){
    rl.question(`Height: `, height => {
        if(!isNaN(height)) {
            console.log(`Rectangular Prims Volume is ${calculateRectangularPrism(length, width, height)}`)
            rl.close()
        } else {
            console.log("Height must be number!");
            inputHeight(length, width)
            }
        
    })
}
inputLength()


/* PR COBA CARA KE DUA, MASUKIN EMPTYSPACE, MASUKIN ENTER, DI FOTO ADA function input() {
    rl.question(`Length: `, length => {
        rl.question(`Width: `, width => {
            rl.question(`Height: `)
        })
    })
} */