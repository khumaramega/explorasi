const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});


function cube(side) {
    return side * side * side
}

const pi = 3.14;
function tube(pi, radius, height) {
    return pi * radius * radius * height
}

function inputsidelength() {
    rl.question(`Side Length: `, side => {
        if (!isNaN(side)) {
            inputradius(side)
        } else {
            console.log("Side Length must be a number\n");
            inputsidelength()
        }
    })
}
    
function inputradius(side) {
        rl.question(`Radius:`, radius => {
            if (!isNaN(radius)) {
                inputheight(side,radius)
            } else {
                console.log("Radius must be a number\n");
                inputradius(side)
            }
        })
}

function inputheight(side, radius) {
    rl.question(`Height:`, height => {
        if (!isNaN(height)) {
            console.log(`Volume of the CUBE is:${cube(side)}. Volume of the TUBE is:${tube(pi,radius, height)}`);
            rl.close()
        } else {
            console.log("Height must be a number\n");
            inputheight(side, radius)
          }
    })
  }
  
  inputsidelength()
