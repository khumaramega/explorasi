// Write if-else statements to fulfill the following conditions:
// if score morethan 60 you pass otherwise you failed
const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})
function isEmptyOrSpaces(str){
    return str === null || str.match(/^ *$/) !== null;
}
function checkRange(num, min, max) {
    return min <= num && num <= max;
  }
function testScore(num) {
    if (num >= 60) {
        return "Passed"
    } else {
        return "Failed"
    }   
}

function inputScore() {
    console.log('N: key-in "q" to quit');
    rl.question("Please key-in your score: ", score => {
    if (score == "q") {
        rl.close()
    } else if (!isNaN(score) && checkRange(score,0,100) && !isEmptyOrSpaces(score)) {
        console.log(`You have ${testScore(score)}\n`);
        inputScore();
    } else {
        console.log("\nPlease key-in only number within 0 - 100\n");
        inputScore();
    }})
}

rl.on("close", () => {
    process.exit()
})

inputScore()

//testScore(50) should return "Failed"
//testScore(90) should return "Passed"
